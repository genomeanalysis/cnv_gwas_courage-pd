# Title: Genome-wide association study of copy number variations in Parkinson's disease

## Study description:
To investigate the effect of copy number variations (CNVs) in Parkinson’s disease (PD), we performed: (1) genome-wide association study of copy number variations (GWAS-CNVs) using the sliding window approach (Collins et al 2022) and (2) a genome-wide burden study using CNV data from 11,035 PD patients and 8,901 controls from the COURAGE-PD consortium.

## Codes available for:
1.	Copy number variant calling, quality control (QC), filtering and formating for rCNV2
2.	Enrichement of biological pathways and diseases
3.	Genome-wide burden
4.	Survival analysis

## Genotyping dataset and QC:

Genotyping QC was conducted by following the standard procedure previously reported in Grover et al 2022 for COURAGE-PD dataset available in (https://github.com/CGEatTuebingen/Ageatonset_GWAS_Courage-PD)

## Sliding windows CNV analysis, assessment of genome-wide significance and association fine-mapping:

The analysis was performed using the rCNV docker (https://hub.docker.com/r/talkowski/rcnv) available under the Talkowski Laboratory (Massachusetts General Hospital & The Broad Institute) repository in Zenodo (https://github.com/talkowski-lab/rCNV2/tree/v1.0, with https://zenodo.org/records/6647918).